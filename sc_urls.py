
from sc_settings import api_base

urls = {
    'users': 'users',
    'streams': 'streams',
    'messages': 'messages'
}

urls = {name: (api_base + url + '.json') for (name, url) in urls.items()}