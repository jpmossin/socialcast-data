
# cleaned up example from repl history

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# read the usesr data
users = pd.read_json('./data/users.json')
users = users[['active', 'followers_count', 'following_count', 'id', 'name', 'title']]

# read message data
messages = pd.read_json('./data/messages.json')
# the user field is a dict; extract only the id
messages['user'] = messages['user'].apply(lambda u: u['id'])
messages = messages[['body', 'likes_count', 'user', 'comments_count', 'id']]

# compute likes per user
likes = messages.groupby("user")["likes_count"].agg(np.sum)
# merge with the user data so that we get full names
likes = pd.merge(likes.to_frame(), users, left_index=True, right_on='id')[['name', 'likes_count']]
# sort by most likes
likes = likes.sort_values("likes_count", ascending=False)

# plot
likes = likes.set_index('name')
likes[:10].plot(kind='bar', rot=45)
plt.show()
