from sc_urls import urls
import sc_api
import json
import os


def create_data_dir(filename):
    if not os.path.exists(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))


def save_to_file(json_data, filename):
    filename = './data/' + filename
    create_data_dir(filename)
    with open(filename, 'w+') as f:
        json.dump(json_data, f)


def get_users():
    return sc_api.get(urls['users'], 'users', {'per_page': 500})


def get_streams():
    return sc_api.get(urls['streams'], 'streams')


def get_messages(stream_id, max_pages=3):
    return sc_api.get_pages(urls['messages'], 'messages_next_page',
                            max_pages=max_pages, field_to_retrieve='messages',
                            request_params={'stream': stream_id, 'comments_limit': 0})


if __name__ == '__main__':
    save_to_file(get_users(), 'users.json')
    streams = get_streams()
    company_stream_id = next(stream for stream in streams if stream['name'] == 'Company')['id']
    save_to_file(get_messages(company_stream_id), 'messages.json')
