import sc_settings
import requests
from requests.auth import HTTPBasicAuth


def get(url, field_to_retrieve=None, request_params=None):
    auth = HTTPBasicAuth(sc_settings.user, sc_settings.pwd)
    req = requests.get(url, params=request_params, auth=auth)
    if field_to_retrieve:
        return req.json()[field_to_retrieve]
    return req.json()


def get_pages(url, next_page_field, max_pages=1e10, field_to_retrieve=None, request_params=None):

    def get_page(page):
        request_params['page'] = page
        resp = get(url, request_params=request_params)
        page = resp.get(next_page_field)
        return (resp[field_to_retrieve], page)

    next_page = 1
    request_params = request_params if request_params else {}
    concat_result = []
    while next_page and next_page < max_pages:
        (data, next_page) = get_page(next_page)
        concat_result.extend(data)
    return concat_result
